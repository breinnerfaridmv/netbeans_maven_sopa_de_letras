package Negocio;

import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetras
{
    // instance variables - replace the example below with your own
    private String sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras()
    {
        
    }

   public SopaDeLetras(String nombreArchivo) throws Exception {
    HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(nombreArchivo));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum()+1;
         //Se crea la matriz con la cantidad de filas de la matriz en excel
         sopas= new String [canFilas][];
        
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            int cantCol=filas.getLastCellNum();
            //Por cada fila se crea la cantidad de columnas
            sopas[i]=new String[cantCol];
            
        for(int j=0;j<cantCol;j++)    
        {
            //Obtiene la celda y su valor respectivo
            sopas[i][j]=filas.getCell(j).getStringCellValue();
            
        }
       }
   }
    
    public void imprimirSopa(){
        for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                System.out.print(sopas[i][j] + "\t");
            }
            System.out.println();
        }
      
    }
    
    /*
    public SopaDeLetras(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }
        
     //Crear la matriz con las correspondientes filas:
     
     String palabras2[]=palabras.split(",");
     this.sopas=new char[palabras2.length][];
     int i=0;
     for(String palabraX:palabras2)
     {
         //Creando las columnas de la fila i
         this.sopas[i]=new char[palabraX.length()];
         pasar(palabraX,this.sopas[i]);
         i++;
        
     }
    }
    */
    private void pasar (String palabra, char fila[])
    {
    
        for(int j=0;j<palabra.length();j++)
        {
            fila[j]=palabra.charAt(j);
        }
    }
    
    
    public String toString()
    {
    String msg="";
    for(int i=0;i<this.sopas.length;i++)
    {
        for (int j=0;j<this.sopas[i].length;j++)
        {
            msg+=this.sopas[i][j]+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    public String toString2()
    {
    String msg="";
    for(String filas[]:this.sopas)
    {
        for (String dato :filas)
        {
            msg+=dato+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    
    public boolean esCuadrada()
    {
        boolean esCuadrada = true;
        if(this.sopas != null){
            int filas = this.sopas.length;
            int columnas = 0;
            for(int i = 0 ; i<this.sopas.length && esCuadrada;i++){
                columnas = this.sopas[i].length;
                if(filas == columnas){
                    esCuadrada = true;
                }
                else{
                    esCuadrada = false;
                }
            }       
        }
        return esCuadrada;

    }
    
    
     public boolean esDispersa()
    {
      boolean esDispersa = false;
        if(!esCuadrada() && !esRectangular()) esDispersa = true;
        return esDispersa;
    }
    
    public boolean esRectangular()
    {
        boolean esRectangular =  true;
        if(this.sopas != null){
            int filas =  this.sopas.length;
            int columnas1 = 0;
            int columnas2 = 0;
            for(int i = 0 ; i<this.sopas.length-1 && esRectangular;i++){
                columnas1= this.sopas[i].length;
                columnas2 = this.sopas[i+1].length;
                if(filas != columnas1 && columnas1 == columnas2){
                    esRectangular =  true;
                }
                else{
                    esRectangular = false;
                }
            }
        }
        return esRectangular;
    }
    
    
    
    /*
        retorna cuantas veces esta la palabra en la matriz
       */
    public int getContar(String palabra)
    {
        return 0;
    }
    
    
    /*
        debe ser cuadrada sopas
       */
    public char []getDiagonalPrincipal() throws Exception
    {
        return  null;
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopas*/
    public String [][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
}
